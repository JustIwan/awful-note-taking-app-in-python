# awful note taking app in python

I'm just trying to learn Python and decided to do this cli-based note taking app and I'll try to get used to working with git along the way.

### Setup:
So, there's nothing spectacular about the setup, simply execute ```python awfulnotes.py``` in a bash shell but make sure your terminal is in the same directory as the awfulnotes.py file!

Obviously, the project is still very barebones and for now, is primarily being developped for Linux but MacOS with a bash shell could theoretically work as well (It doesn't for some reason, macOS could have problems with opening the text editor with ```os.system($EDITOR)``` but feel free to try it anyway.) 


### Troubleshooting:
Did an error regarding the text editor send you here? To fix this, you need to open the ```awfulnotes.py``` file and simply add the text editor of your choice to the ```commoneditors``` array or simply edit your ```.bashrc``` file to contain the following line: ```export EDITOR=yourtexteditorhere```. This could even be the better way.


### "Goals":
The next "big" goal would be to make to make the program look at least somehow pretty because it isn't very pleasent to look at it right now.