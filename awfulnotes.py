import os
import time
import shutil


def checkifeditordefined():
    global youreditor
    commoneditors = ["nano", "vim", "gedit", "emacs", "nvim"]
    syseditorvar = os.getenv("EDITOR")
    if syseditorvar:
        youreditor = "$EDITOR"
    else:
        for i in range(0, len(commoneditors)):
            if shutil.which(commoneditors[i]) is not None:
                youreditor = commoneditors[i]
                break
            else:
                continue
        else:
            print("Error: No text editor found, read the README for troubleshooting.") 
            return       

def actuallycreatethenote(fldir):
    fl = open(fldir, 'x')
    fl.close()
    bashcmd = str(youreditor + " " + fldir)
    os.system(bashcmd)
    print(" \n")

def createdir():
    startdir = "notetxts"
    if not os.path.exists(startdir):
        os.makedirs(startdir)

def countnotes():
    global txt_files
    txt_files = [file for file in os.listdir("notetxts/") if file.endswith(".md")]
    dajskdlenstring = str(len(txt_files))
    print("You currently have " + dajskdlenstring + " notes") 

def listnotes():
    for i in range(0, len(txt_files)):
        timestamp = int(os.path.getmtime("notetxts/" + txt_files[i]))
        currenttime = int(time.time())
        timedifference = currenttime - timestamp
        diffindays = timedifference/(60*60*24)
        daysrest = diffindays - int(diffindays)
        days = int(diffindays)
        restinhours = daysrest*24
        hoursrest = restinhours - int(restinhours)
        hours = int(restinhours)
        restinminutes = hoursrest*60
        minutesrest = restinminutes - int(restinminutes)
        minutes = int(restinminutes)
        seconds = int(minutesrest*60)
        timesarray = [days, hours, minutes, seconds]
        timeswordarray = ["days", "hours", "minutes", "seconds"]
        returntimesincelastedited(timesarray, timeswordarray)
        print(txt_files[i] + "  " + str(i+1) + ")    [" + lasteditedvar + "]")

def returntimesincelastedited(timesarray, timeswordarray):
    for i in range(0, len(timesarray)):
        if timesarray[i] == 0:
            continue
        else:
            global lasteditedvar
            lasteditedvar = str("last edited " + str(timesarray[i]) + " " + timeswordarray[i] + " ago")
            break

def erralrexist(notename):
    uhisd3 = input("A note with this name already exists. \n Do you want to replace the old one ('r') or create a new one? ('n') \n  ")
    if uhisd3 == "r":
        fldir = str("notetxts/" +  notename + ".md")
        os.remove(fldir) 
        actuallycreatethenote(fldir)
        introduction()
    elif uhisd3 == "n":
        createnewnote()
    else:
        print("Please choose what to do")
        erralrexist()

def createnewnote():
    notename = input("How would you like to name your new note? \n Type 'b' to go back \n  ")    
    if notename == "b":
        introduction()
    else:

        fldir = str("notetxts/" +  notename + ".md")
        if os.path.exists(fldir):
            erralrexist(notename)
        else:
            actuallycreatethenote(fldir)
            introduction()

def choooserenamedeletefunc(onotedir):
    choooserenamedelete = input("What do you want to do with the note?\n Open ('o'), rename ('r') or delete ('d') it? Or type 'b' to go back\n ")
    if choooserenamedelete == "o":
        os.system(youreditor + " " + onotedir)
        oldnotesmenu()
    elif choooserenamedelete == "d":
        os.remove(onotedir)
        oldnotesmenu()
    elif choooserenamedelete == "r":
        renamename = input("How would you like to rename the note?\n")
        os.rename(onotedir, "notetxts/" + str(renamename) + ".md")
        oldnotesmenu()
    else:
        choooserenamedeletefunc(onotedir)
        print("Choose a valid option \n")

def menu2inoldnotes(oldoteschoose):
    try:
        var23 = int(oldoteschoose) - 1
        if 0<= var23 < len(txt_files):
            onotedir = "notetxts/" + txt_files[var23]
            choooserenamedeletefunc(onotedir)
        else:
            print("Please enter a valid number \n")
            oldnotesmenu()
    except ValueError:
        print("Please enter a valid number \n")
        oldnotesmenu()

def oldnotesmenu():
    print(" ")
    countnotes()
    listnotes()
    oldoteschoose = input("Type in the number of the note you want to access or 'b' to go back \n  ")
    if oldoteschoose == "b":
        introduction()
    else:
        menu2inoldnotes(oldoteschoose)

def introduction():
    intro732 = input("Welcome back! Do you want to look at your old notes (type 'o') or start something new? (type 'n')\n Type 'q' to quit the application \n  ")
    if intro732 == "o":
        oldnotesmenu()
    elif intro732 == "n":
        createnewnote()
    elif intro732 == "q":
        return
    else:
        print("Please select an option \n") 
        introduction()

def startprogram():
    checkifeditordefined()
    createdir()
    introduction()

startprogram()
